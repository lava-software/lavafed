from django.conf.urls import include, url
from django.contrib import admin
from lavafed import urls as fed_urls

urlpatterns = [url(r"^admin/", admin.site.urls), url(r"^", include(fed_urls))]
