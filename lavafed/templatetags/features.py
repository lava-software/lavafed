# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2019 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from django import template

from lavafed.models import Job, Feature

register = template.Library()


@register.simple_tag
def features(dt, version, complete, type_str, action_str):
    if complete:
        healths = [Job.HEALTH_COMPLETE]
    else:
        healths = [Job.HEALTH_INCOMPLETE, Job.HEALTH_CANCELED]
    return Feature.objects.filter(
        job__version=version,
        job__health__in=healths,
        job__device__dt=dt,
        type=Feature.reverse_type(type_str),
        action=Feature.reverse_action(action_str),
    ).distinct()


@register.simple_tag
def feature_results(feature, version):
    return feature.job_set.filter(version=version)
