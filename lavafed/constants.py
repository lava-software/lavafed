# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

LOG_FORMAT = "%(asctime)-15s %(levelname)7s %(message)s"

CFG_DURATION_RE = r"((?P<hour>\d+)h)*((?P<minute>\d+)m)*"
CFG_STARTING_RE = r"(?P<hour>\d{1,2}):(?P<minute>\d{2})"

VERSION_RE = r"(?P<tag>\d{4}\.\d+)\.(?P<commits>\d+)\.g(?P<hash>\w{9})"
VERSION_TAGS_RE = r"(?P<tag>\d{4}\.\d+)"
