# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-30 10:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("lavafed", "0006_lab_url")]

    operations = [
        migrations.AddField(
            model_name="job",
            name="created",
            field=models.DateTimeField(
                auto_now_add=True, default=django.utils.timezone.now
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="job",
            name="lava",
            field=models.IntegerField(default=0, verbose_name="Lava job id"),
        ),
        migrations.AlterField(
            model_name="job",
            name="type",
            field=models.IntegerField(
                choices=[(0, "FED"), (1, "SLAVE"), (2, "META"), (3, "PACKAGE")],
                default=0,
            ),
        ),
    ]
