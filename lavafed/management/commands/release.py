# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import contextlib
import pathlib
import time
import xmlrpc.client

from lavafed.cmdutils import LAVAFedCommand
from lavafed.constants import LOG_FORMAT
from lavafed.models import Device, Job, Lab, Version
from lavafed.utils import (
    build_proxy,
    suppress404,
    wait_for_devices,
    build_docker_version,
)


class Command(LAVAFedCommand):
    help = "release lab device"

    def add_arguments(self, parser):
        log = parser.add_argument_group("logging")
        log.add_argument(
            "-l",
            "--level",
            default="DEBUG",
            help="Logging level (ERROR, WARN, INFO, DEBUG) " "Default: DEBUG",
        )

        log.add_argument("-o", "--log-file", default="-", help="Logging file path")

        parser.add_argument(
            "--config",
            "-c",
            default="/etc/lavafed",
            help="configuration directory",
            metavar="DIR",
            type=pathlib.Path,
        )
        parser.add_argument(
            "--force",
            default=False,
            action="store_true",
            help="acquire even outside of the slot",
        )
        parser.add_argument("lab", help="lab name", type=str)

    def handle(self, *args, **kwargs):
        # Initialize logging.
        self.setup_logging("lavafed", kwargs["level"], kwargs["log_file"], LOG_FORMAT)

        self.logger.info("[INIT] Loading config '%s'", kwargs["config"].absolute())
        self.logger.debug("[INIT] * master.yaml")
        master_cfg_path = kwargs["config"] / "master.yaml"
        if not master_cfg_path.exists():
            self.logger.error("[INIT] => file does not exist")
            return
        self.master_cfg = self.load_configuration(master_cfg_path, master=True)
        if self.master_cfg is None:
            self.logger.error("[INIT] => invalid configuration")
            return

        self.logger.debug("[INIT] * labs/%s/config.yaml", kwargs["lab"])
        lab_cfg_path = kwargs["config"] / "labs" / kwargs["lab"] / "config.yaml"
        if not lab_cfg_path.exists():
            self.logger.error("[INIT] => file does not exist")
            return
        self.lab_cfg = self.load_configuration(lab_cfg_path)
        if self.lab_cfg is None:
            self.logger.error("[INIT] => invalid configuration")
            return

        # Setup xmlrpc proxies
        self.logger.info("[INIT] Start xmlrpc proxies")
        self.master = build_proxy(self.master_cfg)
        self.master_version = self.master.system.version()
        self.logger.info("[INIT] * master %s", self.master_version)
        self.lab = build_proxy(self.lab_cfg)
        self.lab_version = self.lab.system.version()
        self.logger.info("[INIT] * lab %s", self.lab_version)

        # Check if we are outside of the allowed time slot
        if self.lab_cfg["slot"].is_allowed():
            self.logger.info("Still in the time slot")
            if not kwargs["force"]:
                return
            self.logger.warning("Release anyway")

        # Get version and lab objects
        docker_version = build_docker_version(self.master_version)
        version, _ = Version.objects.get_or_create(version=docker_version.lstrip("/:"))
        lab, _ = Lab.objects.get_or_create(name=kwargs["lab"])

        self.logger.info("[master] cancel remaining jobs")
        for job in Job.objects.filter(
            device__lab=lab,
            version=version,
            type=Job.TYPE_FED,
            health=Job.HEALTH_UNKNOWN,
        ):
            try:
                self.logger.info("- %s (%d)", job.name, job.lava)
                self.master.scheduler.jobs.cancel(job.lava)
            except xmlrpc.client.Fault as exc:
                if exc.faultCode == 404:
                    self.logger.warning("--> not found")
                else:
                    # TODO: return the error code
                    self.logger.error("--> unable to cancel")
                    self.logger.exception(exc)

        self.logger.info("[master] release devices:")
        for device in Device.objects.filter(lab=lab, acquired=True).exclude(
            dt__name="lava-slave"
        ):
            d_name = device.master_name()
            self.logger.info("- %s", d_name)
            with suppress404(self.logger):
                if (
                    self.master.scheduler.devices.show(d_name)["health"]
                    == "Maintenance"
                ):
                    self.logger.info("--> already done")
                    continue
                self.master.scheduler.devices.update(
                    d_name, None, None, None, None, "MAINTENANCE", None
                )

        self.logger.info("[master] waiting for jobs to end:")
        devices = list(
            Device.objects.filter(lab=lab, acquired=True).exclude(dt__name="lava-slave")
        )
        for device in wait_for_devices(self.master, self.logger, devices, True):
            self.logger.info("--> [lab] release '%s'", device.name)
            with suppress404(self.logger):
                # Set health to the health when acquiring
                self.lab.scheduler.devices.update(
                    device.name,
                    None,
                    None,
                    None,
                    None,
                    device.get_health_display().upper(),
                    None,
                )
                # Remove device from the api
                device.acquired = False
                device.save()

        self.logger.info("[lab] cancel slave")
        with contextlib.suppress(Job.DoesNotExist):
            slave = Job.objects.get(
                device__lab=lab,
                version=version,
                type=Job.TYPE_SLAVE,
                health=Job.HEALTH_UNKNOWN,
            )
            self.logger.info("-> %d", slave.lava)
            self.lab.scheduler.jobs.cancel(slave.lava)
            while self.lab.scheduler.jobs.show(slave.lava)["state"] != "Finished":
                self.logger.info("-> waiting")
                time.sleep(15)
        self.logger.info("-> done")
