# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import contextlib
import pathlib
import time
import xmlrpc.client
import yaml

from lavafed.cmdutils import LAVAFedCommand
from lavafed.constants import LOG_FORMAT
from lavafed.models import Device, DeviceType, Job, Lab, Version
from lavafed.utils import (
    build_docker_version,
    build_proxy,
    copy_device,
    render_template,
    suppress404,
    wait_for_devices,
)


class Command(LAVAFedCommand):
    help = "acquire lab device"

    def add_arguments(self, parser):
        log = parser.add_argument_group("logging")
        log.add_argument(
            "-l",
            "--level",
            default="DEBUG",
            help="Logging level (ERROR, WARN, INFO, DEBUG) " "Default: DEBUG",
        )

        log.add_argument("-o", "--log-file", default="-", help="Logging file path")

        parser.add_argument(
            "--config",
            "-c",
            default="/etc/lavafed",
            help="configuration directory",
            metavar="DIR",
            type=pathlib.Path,
        )
        parser.add_argument(
            "--resubmit-slave",
            default=False,
            action="store_true",
            help="resubmit slave job (if needed)",
        )
        parser.add_argument("lab", help="lab name", type=str)

    def start_slave(self, options, docker_version):
        version, _ = Version.objects.get_or_create(version=docker_version.lstrip("/:"))
        lab, _ = Lab.objects.get_or_create(name=options["lab"])
        dt, _ = DeviceType.objects.get_or_create(name="lava-slave")
        device, _ = Device.objects.get_or_create(name="lava-slave", dt=dt, lab=lab)

        with contextlib.suppress(Job.DoesNotExist):
            job = Job.objects.get(
                name="lava-slave", device=device, version=version, type=Job.TYPE_SLAVE
            )
            self.logger.info("[lab] slave already submitted")
            self.logger.debug("-> id: %s", job.lava)
            if not options["resubmit_slave"]:
                return job
            self.logger.warning("-> resubmit")
            job.delete()

        self.logger.info("[lab] submit slave")
        remaining_time = self.lab_cfg["slot"].remaining_time()
        context = {
            "api_url": self.master_cfg["api"]["url"],
            "api_token": self.master_cfg["api"]["token"],
            "job_url": self.lab_cfg["job_url"],
            "docker_version": docker_version,
            "job_timeout": remaining_time,
            "lab_name": options["lab"],
            "slave_arch": self.lab_cfg["arch"],
            "worker_token": self.lab_cfg["worker_token"],
            "slave_version": version.version,
            "timeout": max(30, remaining_time - 120),  # TODO this should be automatic
        }
        job_def = render_template(
            options["config"] / "labs" / options["lab"] / "jobs" / "lava-slave.jinja2",
            options["config"] / "templates",
            **context
        )
        job_id = self.lab.scheduler.jobs.submit(str(job_def))
        self.logger.debug("-> id: %s", job_id)
        return Job.objects.create(
            name="lava-slave",
            device=device,
            version=version,
            type=Job.TYPE_SLAVE,
            lava=job_id,
            arch=Job.ARCH_REVERSE[self.lab_cfg["arch"]],
        )

    def handle(self, *args, **kwargs):
        # Initialize logging.
        self.setup_logging("lavafed", kwargs["level"], kwargs["log_file"], LOG_FORMAT)

        self.logger.info("[INIT] Loading config '%s'", kwargs["config"].absolute())
        self.logger.debug("[INIT] * master.yaml")
        master_cfg_path = kwargs["config"] / "master.yaml"
        if not master_cfg_path.exists():
            self.logger.error("[INIT] => file does not exist")
            return
        self.master_cfg = self.load_configuration(master_cfg_path, master=True)
        if self.master_cfg is None:
            self.logger.error("[INIT] => invalid configuration")
            return

        self.logger.debug("[INIT] * labs/%s/config.yaml", kwargs["lab"])
        lab_cfg_path = kwargs["config"] / "labs" / kwargs["lab"] / "config.yaml"
        if not lab_cfg_path.exists():
            self.logger.error("[INIT] => file does not exist")
            return
        self.lab_cfg = self.load_configuration(lab_cfg_path)
        if self.lab_cfg is None:
            self.logger.error("[INIT] => invalid configuration")
            return

        # Setup xmlrpc proxies
        self.logger.info("[INIT] Start xmlrpc proxies")
        self.master = build_proxy(self.master_cfg)
        self.master_version = self.master.system.version()
        self.logger.info("[INIT] * master %s", self.master_version)
        self.lab = build_proxy(self.lab_cfg)
        self.lab_version = self.lab.system.version()
        self.logger.info("[INIT] * lab %s", self.lab_version)

        # Check if we are in the allowed time slot
        if not self.lab_cfg["slot"].is_allowed():
            self.logger.info("Not allowed to run now")
            return

        # Start the slave if needed
        docker_version = build_docker_version(self.master_version)
        job = self.start_slave(kwargs, docker_version)

        # Wait for the slave to be online!
        while True:
            with contextlib.suppress(xmlrpc.client.Fault):
                ret = self.master.scheduler.workers.show(kwargs["lab"])
                # TODO: check that ret["last_ping"] is < 20s
                if ret["state"] == "Online":
                    break
            with contextlib.suppress(xmlrpc.client.Fault):
                # Check that the job is running
                ret = self.lab.scheduler.jobs.show(job.lava)
                if ret["state"] == "Finished":
                    self.logger.error("--> slave was not able to connect")
                    return
            time.sleep(15)
        # mark the device slave as acquired
        job.device.acquired = True
        job.device.health = Device.HEALTH_GOOD
        job.device.save()

        # Add the slave configuration
        if "dispatcher" in self.lab_cfg:
            config = yaml.safe_dump(self.lab_cfg["dispatcher"])
            self.logger.info("[lab] Setting dispatcher configuration")
            self.logger.debug("-> %s", config.rstrip("\n"))
            self.master.scheduler.workers.set_config(kwargs["lab"], config)
        # Add the slave configuration
        if "env" in self.lab_cfg:
            config = yaml.safe_dump(self.lab_cfg["env"])
            self.logger.info("[lab] Setting dispatcher env")
            self.logger.debug("-> %s", config.rstrip("\n"))
            self.master.scheduler.workers.set_env(kwargs["lab"], config)

        self.logger.info("[lab] maintenance:")

        # List devices already acquired
        version = Version.objects.get(version=docker_version.lstrip("/:"))
        lab = Lab.objects.get(name=kwargs["lab"])
        acquired = list(
            Device.objects.filter(lab=lab, acquired=True)
            .exclude(dt__name="lava-slave")
            .values_list("name", flat=True)
        )
        for device in self.lab_cfg["devices"]:
            device_name = device["name"]
            if device_name in acquired:
                self.logger.info("- %s [already added]", device_name)
                continue

            # Add the device
            self.logger.info("- %s", device_name)
            data = self.lab.scheduler.devices.show(device_name)
            health = data["health"]
            if health not in ["Good", "Unknown"]:
                # TODO: send a mail to admins?
                self.logger.warning("--> health is not good, skip (%s)", health)
                continue

            with suppress404(self.logger):
                self.lab.scheduler.devices.update(
                    device_name, None, None, None, None, "MAINTENANCE", None
                )
                # Record the device health
                # Patch the device-type if needed
                dt_name = device.get("device-type", data["device_type"])
                dt, _ = DeviceType.objects.get_or_create(name=dt_name)
                device, _ = Device.objects.get_or_create(
                    name=device_name, dt=dt, lab=lab
                )
                device.health = Device.HEALTH_REVERSE[health]
                device.acquired = True
                device.save()

        self.logger.info("[master] add in maintenance:")
        # Use the devices list from the db
        for device in Device.objects.filter(lab=lab, acquired=True).exclude(
            dt__name="lava-slave"
        ):
            self.logger.info("- %s", device.name)
            copy_device(
                device, self.master, self.lab, self.lab_cfg["devices"], self.logger
            )

        self.logger.info("[master] submit:")
        lab_dir = kwargs["config"] / "labs" / kwargs["lab"]
        for device in Device.objects.filter(lab=lab, acquired=True).exclude(
            dt__name="lava-slave"
        ):
            self.logger.info("- %s", device.name)
            jobs = (lab_dir / "jobs" / device.name).glob("*.jinja2")
            submitted_jobs = list(
                Job.objects.filter(device=device, version=version).values_list(
                    "name", flat=True
                )
            )
            for job in sorted(jobs):
                context = {
                    "tags": [kwargs["lab"]],
                    "lab_name": kwargs["lab"],
                    "device_name": device.name,
                    "slave_arch": self.lab_cfg["arch"],
                    "slave_version": version.version,
                    "api_url": self.master_cfg["api"]["url"],
                    "api_token": self.master_cfg["api"]["token"],
                    "job_url": self.master_cfg["job_url"],
                }
                job_def = render_template(
                    job, kwargs["config"] / "templates", **context
                )

                if job_def is None:
                    self.logger.error("--> Unable to render template '%s'", job)
                    continue

                # Load job name from job definition
                job_name = yaml.safe_load(job_def)["metadata"]["job.name"]

                self.logger.info("--> %s (%s)", job.name, job_name)
                if job_name in submitted_jobs:
                    self.logger.info("---> already submitted")
                    continue

                job_id = self.master.scheduler.jobs.submit(job_def)
                self.logger.debug("---> %s", job_id)
                # record the job
                Job.objects.create(
                    name=job_name,
                    lava=job_id,
                    device=device,
                    version=version,
                    arch=Job.ARCH_REVERSE[self.lab_cfg["arch"]],
                )

        self.logger.info("[lab] waiting for jobs to end:")
        devices = list(
            Device.objects.filter(lab=lab, acquired=True).exclude(dt__name="lava-slave")
        )

        for device in wait_for_devices(self.lab, self.logger, devices, False):
            device_name = device.master_name()
            self.logger.info("--> [master] acquire '%s'", device_name)
            if (
                self.master.scheduler.devices.show(device_name)["health"]
                == "Maintenance"
            ):
                self.master.scheduler.devices.update(
                    device_name, None, None, None, None, "UNKNOWN", None
                )
        return
