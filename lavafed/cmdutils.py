# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import logging
import yaml

from .schema import lab_schema, master_schema
from .utils import Slot

from django.core.management.base import BaseCommand


class LAVAFedCommand(BaseCommand):
    def load_configuration(self, path, master=False):
        config = None
        try:
            config = yaml.safe_load(path.read_text(encoding="utf-8"))
        except yaml.YAMLError as exc:
            self.logger.exception(exc)
            return None

        schema = master_schema() if master else lab_schema()
        try:
            schema(config)
        except Exception as exc:
            self.logger.exception(exc)
            return None

        if "slot" in config:
            config["slot"] = Slot(config["slot"])
        return config

    def setup_logging(self, logger_name, level, log_file, log_format):
        # Create the logger
        self.logger = logging.getLogger(logger_name)
        if log_file == "-":
            handler = logging.StreamHandler()
        else:
            handler = logging.handlers.WatchedFileHandler(log_file)
        handler.setFormatter(logging.Formatter(log_format))
        self.logger.addHandler(handler)

        # Set log level
        if level == "ERROR":
            self.logger.setLevel(logging.ERROR)
        elif level == "WARN":
            self.logger.setLevel(logging.WARN)
        elif level == "INFO":
            self.logger.setLevel(logging.INFO)
        else:
            self.logger.setLevel(logging.DEBUG)
