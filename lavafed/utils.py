# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import contextlib
import datetime
import jinja2
import pytz
import re
import requests
import time
from urllib.parse import urlparse
import xmlrpc.client

from django.utils import timezone

from .constants import CFG_DURATION_RE, CFG_STARTING_RE, VERSION_TAGS_RE, VERSION_RE


###########
# Decorator
###########
@contextlib.contextmanager
def suppress404(logger):
    try:
        yield
    except xmlrpc.client.Fault as exc:
        if exc.faultCode != 404:
            raise
        if logger is not None:
            logger.warning("--> not found")


#######
# Class
#######
class RequestsTransport(xmlrpc.client.Transport):
    def __init__(self, scheme):
        super().__init__()
        self.scheme = scheme

    def request(self, host, handler, request_body, verbose=False):
        headers = {
            "User-Agent": "lavafed",
            "Content-Type": "text/xml",
            "Accept-Encoding": "gzip",
        }
        url = "%s://%s%s" % (self.scheme, host, handler)
        try:
            response = None
            response = requests.post(
                url, data=request_body, headers=headers, timeout=20
            )
            response.raise_for_status()
            return self.parse_response(response)
        except xmlrpc.client.Fault:
            raise
        except Exception as e:
            if response is None:
                raise xmlrpc.client.ProtocolError(url, 500, str(e), "")
            else:
                raise xmlrpc.client.ProtocolError(
                    url, response.status_code, str(e), response.headers
                )

    def parse_response(self, response):
        """
        Parse the xmlrpc response.
        """
        p, u = self.getparser()
        p.feed(response.text)
        p.close()
        return u.close()


class Slot:
    def __init__(self, data):
        self.every = data["every"]
        self.starting = data["starting"]
        self.timezone = data["timezone"]
        self.duration = data["duration"]

    DAYS = {
        "monday": 0,
        "tuesday": 1,
        "wednesday": 2,
        "thursday": 3,
        "friday": 4,
        "saturday": 5,
        "sunday": 6,
    }

    def is_allowed(self):
        timezone.activate(pytz.timezone(self.timezone))
        now = timezone.now()

        # Check the current day
        # FIXME: this won't work if duration span other two days
        if self.every.lower() in self.DAYS.keys():
            weekday = self.DAYS[self.every.lower()]
            if now.weekday() != weekday:
                return False
        elif self.every.lower() == "weekday":
            if now.weekday() in [5, 6]:
                return False
        elif self.every.lower() == "weekend":
            if now.weekday() not in [5, 6]:
                return False
        elif self.every.lower() == "day":
            pass

        # Check the current time
        m = re.compile(CFG_STARTING_RE).match(self.starting).groupdict()
        start = timezone.make_aware(
            datetime.datetime(
                year=now.year,
                month=now.month,
                day=now.day,
                hour=int(m["hour"]),
                minute=int(m["minute"]),
            )
        )
        m = re.compile(CFG_DURATION_RE).match(self.duration).groupdict()
        seconds = int(m["hour"]) * 3600 if m["hour"] is not None else 0
        seconds += int(m["minute"]) * 60 if m["minute"] is not None else 0
        # TODO: remove some minutes from the end to allow some time for canceling jobs
        end = start + datetime.timedelta(seconds=seconds)
        return bool(start <= now <= end)

    def remaining_time(self):
        timezone.activate(pytz.timezone(self.timezone))
        now = timezone.now()

        # Check the current time
        start_hour, start_minute = self.starting.split(":")
        start = timezone.make_aware(
            datetime.datetime(
                year=now.year,
                month=now.month,
                day=now.day,
                hour=int(start_hour),
                minute=int(start_minute),
            )
        )
        m = (
            re.compile(r"((?P<hour>\d+)h)*((?P<minute>\d+)m)*")
            .match(self.duration)
            .groupdict()
        )
        seconds = int(m["hour"]) * 3600 if m["hour"] is not None else 0
        seconds += int(m["minute"]) * 60 if m["minute"] is not None else 0
        end = start + datetime.timedelta(seconds=seconds)
        return int(max(0, (end - now).total_seconds()))


#########
# Helpers
#########
def build_docker_version(version):
    match = re.compile(VERSION_RE).match(version)
    if match is not None:
        data = match.groupdict()
        return "%s.%s.g%s" % (data["tag"], data["commits"], data["hash"])

    match = re.compile(VERSION_TAGS_RE).match(version)
    if match is not None:
        data = match.groupdict()
        return match.groupdict()["tag"]
    raise Exception("Unable to match version '%s'" % version)


def build_proxy(config):
    (scheme, uri) = build_uri(config["url"], config["username"], config["token"])
    return xmlrpc.client.ServerProxy(
        uri, allow_none=True, transport=RequestsTransport(scheme)
    )


def build_uri(url, username, token):
    p = urlparse(url)
    uri = "%s://%s:%s@%s%s" % (p.scheme, username, token, p.netloc, p.path)
    return (p.scheme, uri)


def get_device_config(lab_config, name):
    for d in lab_config:
        if d["name"] == name:
            return d


def copy_device(device, master, lab, devices_config, logger):
    # TODO: lab_name worker should exists before calling this!
    device_config = lab.scheduler.devices.show(device.name)
    device_dict = str(lab.scheduler.devices.get_dictionary(device.name, False))
    d_name = device.master_name()
    device_cfg = get_device_config(devices_config, device.name)

    # Check that the device-type exist
    # If "device-type" is specified in the device config use it and defaults to
    # the name coming from the api.
    dt = device_cfg.get("device-type", device_config["device_type"])

    if dt not in [d["name"] for d in master.scheduler.device_types.list()]:
        logger.debug("--> create device-type '%s'", dt)
        master.scheduler.device_types.add(dt, "", True, False, 24, "hours")

    # Check that the device exists
    devices = master.scheduler.devices.list()
    if d_name not in [d["hostname"] for d in devices]:
        logger.debug("--> add device as '%s'", d_name)
        # Add in maintenance mode to have some time to add the device dictionary
        master.scheduler.devices.add(
            d_name, dt, device.lab.name, None, None, True, "MAINTENANCE", None
        )
    logger.debug("--> add tag '%s'", device.lab.name)
    master.scheduler.devices.tags.add(d_name, device.lab.name)

    logger.debug("--> update device dict")
    # Patch device dict if needed
    for patch in device_cfg.get("patches", []):
        device_dict = device_dict.replace(patch["from"], patch["to"])
    master.scheduler.devices.set_dictionary(d_name, device_dict)


def render_template(f_template, base_dir, **kwargs):
    env = jinja2.Environment(  # nosec - rendering yaml
        autoescape=False,
        loader=jinja2.FileSystemLoader([str(base_dir)]),
        undefined=jinja2.StrictUndefined,
    )
    data = f_template.read_text(encoding="utf-8")
    template = env.from_string(data)
    try:
        return template.render(**kwargs)
    except jinja2.TemplateNotFound:
        return None


def wait_for_devices(proxy, logger, devices, release):
    wait_for = devices.copy()
    while wait_for:
        next_list = []
        for device in wait_for:
            with suppress404(None):
                device_name = device.master_name() if release else device.name
                ret = proxy.scheduler.devices.show(device_name)
                if ret["current_job"] is not None:
                    next_list.append(device)
                    if release:
                        proxy.scheduler.jobs.cancel(ret["current_job"])
                else:
                    logger.info("- %s", device.name)
                    yield device
        if not next_list:
            return
        time.sleep(20)
        wait_for = next_list
