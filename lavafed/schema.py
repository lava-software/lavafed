# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from voluptuous import Any, Match, Optional, Required, Schema, Url

from .constants import CFG_DURATION_RE, CFG_STARTING_RE


def lab_schema():
    return Schema(
        {
            Required("url"): Url(),
            Required("job_url"): Url(),
            Required("username"): str,
            Required("token"): str,
            Required("arch"): Any("aarch64", "amd64"),
            Required("worker_token"): str,
            # TODO: improve starting and duration parsing
            Required("slot"): {
                Required("every"): Any(
                    "day",
                    "weekday",
                    "weekend",
                    "monday",
                    "tuesday",
                    "wednesday",
                    "thursday",
                    "friday",
                    "saturday",
                    "sunday",
                ),
                Required("starting"): Match(CFG_STARTING_RE),
                Required("timezone"): str,
                Required("duration"): Match(CFG_DURATION_RE),
            },
            Optional("dispatcher"): dict,
            Optional("env"): dict,
            Required("devices"): [
                {
                    Required("name"): str,
                    Optional("patches"): [{Required("from"): str, Required("to"): str}],
                    Optional("device-type"): str,
                }
            ],
        }
    )


def master_schema():
    return Schema(
        {
            Required("job_url"): Url(),
            Required("url"): Url(),
            Required("username"): str,
            Required("token"): str,
            Required("api"): {Required("url"): Url(), Required("token"): str},
        }
    )
