# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls import url

from . import views as v


urlpatterns = [
    url(r"^$", v.home, name="home"),
    url(r"^devices/$", v.devices, name="devices"),
    url(r"^devices/(?P<lab>[^/]+)/$", v.devices, name="devices.lab"),
    url(
        r"^devices/(?P<lab>[^/]+)/(?P<device>[^/]+)/$",
        v.device_show,
        name="device.show",
    ),
    url(r"^device-types/$", v.device_types, name="device_types"),
    url(
        r"^device-types/(?P<name>[^/]+)/$", v.device_type_show, name="device_types.show"
    ),
    url(r"^labs/$", v.labs, name="labs"),
    url(r"^labs/(?P<lab>[^/]+)/$", v.lab_show, name="labs.show"),
    url(r"^versions/$", v.versions, name="versions"),
    url(r"^versions/latest/$", v.version_show_latest, name="versions.show_latest"),
    url(r"^versions/(?P<version>[^/]+)/$", v.version_show, name="versions.show"),
    url(
        r"^versions/latest/badge/$",
        v.version_badge_latest,
        name="versions.badge_latest",
    ),
    url(
        r"^versions/(?P<version>[^/]+)/badge/$", v.version_badge, name="versions.badge"
    ),
    url(r"^api/v0.1/jobs/$", v.api_v0_1_jobs, name="api.v0.1.jobs"),
    url(
        r"^api/v0.1/versions/(?P<version>[^/]+)/$",
        v.api_v0_1_versions_show,
        name="api.v0.1.versions.show",
    ),
]
