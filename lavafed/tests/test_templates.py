# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import jinja2
import pathlib
import pytest
import yaml

from lavafed.utils import render_template


BASE_DIR = pathlib.Path(__file__).parent.parent.parent.absolute()
TEMPLATE_DIR = BASE_DIR / "etc" / "templates"


def test_raise_undefined():
    for f in TEMPLATE_DIR.glob("*.jinja2"):
        if f.name == "base.jinja2":
            continue
        with pytest.raises(jinja2.exceptions.UndefinedError):
            render_template(f, TEMPLATE_DIR)


def test_lava_slave():
    ctx = {
        "api_url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
        "api_token": "my_token",
        "docker_version": "master-2018.10-0018-g08ea617a4",
        "job_timeout": 100,
        "job_url": "https://lava.com/scheduler/job/{id}",
        "lab_name": "lava.example.com",
        "slave_arch": "amd64",
        "worker_token": "worker_token1",
        "slave_version": "master-2018.10-0018-g08ea617a4",
        "timeout": 80,
    }
    data = yaml.safe_load(
        render_template(TEMPLATE_DIR / "lava-slave.jinja2", TEMPLATE_DIR, **ctx)
    )

    assert data["device_type"] == "docker"  # nosec - pytest
    assert (  # nosec - pytest
        data["job_name"] == "[lavafed master-2018.10-0018-g08ea617a4] lava-slave"
    )
    assert data["timeouts"] == {"job": {"seconds": 100}}  # nosec - pytest
    assert data["visibility"] == {"group": ["lavafed"]}  # nosec - pytest
    assert "tags" not in data  # nosec - pytest

    assert data["metadata"] == {  # nosec - pytest
        "device.name": "lava-slave",
        "device.type": "lava-slave",
        "job.name": "lava-slave",
        "job.type": "slave",
        "job.url": "https://lava.com/scheduler/job/{id}",
        "lab.name": "lava.example.com",
        "slave.arch": "amd64",
        "slave.version": "master-2018.10-0018-g08ea617a4",
        "features.0.name": "master-slave",
        "features.0.type": "generic",
        "features.0.description": "master-slave protocol",
        "features.0.action": "None",
    }

    assert data["notify"] == {  # nosec - pytest
        "criteria": {"status": "finished"},
        "callback": {
            "url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
            "token": "my_token",
            "method": "POST",
            "dataset": "all",
            "content-type": "json",
        },
    }


def test_lava_slave_aarch64():
    ctx = {
        "api_url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
        "api_token": "my_token",
        "docker_version": "master-2018.10-0018-g08ea617a4",
        "job_timeout": 100,
        "job_url": "https://lava.com/scheduler/job/{id}",
        "lab_name": "lava.example.com",
        "slave_arch": "aarch64",
        "worker_token": "worker_token1",
        "slave_version": "master-2018.10-0018-g08ea617a4",
        "timeout": 80,
    }
    data = yaml.safe_load(
        render_template(TEMPLATE_DIR / "lava-slave.jinja2", TEMPLATE_DIR, **ctx)
    )
    assert data["metadata"]["slave.arch"] == "aarch64"  # nosec - pytest


def test_jobs():
    base_ctx = {
        "api_url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
        "api_token": "my_token",
        "docker_version": "master-2018.10-0018-g08ea617a4",
        "job_timeout": 100,
        "job_url": "https://lava.com/scheduler/job/{id}",
        "lab_name": "lava.example.com",
        "slave_arch": "amd64",
        "worker_token": "worker_token1",
        "slave_version": "master-2018.10-0018-g08ea617a4",
        "tags": ["hello", "tag"],
        "timeout": 80,
    }

    for f in TEMPLATE_DIR.glob("*.jinja2"):
        # TODO: test lava-slave template
        if f.name in ["base.jinja2", "health-check.jinja2", "lava-slave.jinja2"]:
            continue
        f_stem = f.stem
        if f.name in ["cubietruck-nfs.jinja2", "cubietruck-ramdisk.jinja2"]:
            f_stem = "cubietruck"
        elif f.name == "qemu-armv7.jinja2":
            f_stem = "qemu"

        ctx = base_ctx.copy()
        ctx["device_name"] = f_stem + "-01"
        data = yaml.safe_load(render_template(f, TEMPLATE_DIR, **ctx))

        assert data["device_type"] == f_stem  # nosec - pytest
        assert data["job_name"].startswith(  # nosec - pytest
            "[lavafed master-2018.10-0018-g08ea617a4] " + f_stem
        )
        assert data["timeouts"] == {"job": {"seconds": 100}}  # nosec - pytest
        assert data["visibility"] == "public"  # nosec - pytest
        assert data["tags"] == ["hello", "tag"]  # nosec - pytest

        assert data["metadata"]["device.name"] == f_stem + "-01"  # nosec - pytest
        assert data["metadata"]["device.type"] == f_stem  # nosec - pytest
        assert data["metadata"]["job.type"] == "test"  # nosec - pytest
        assert data["metadata"]["lab.name"] == "lava.example.com"  # nosec - pytest
        assert data["metadata"]["slave.arch"] == "amd64"  # nosec - pytest
        assert (  # nosec - pytest
            data["metadata"]["slave.version"] == "master-2018.10-0018-g08ea617a4"
        )

        assert data["notify"] == {  # nosec - pytest
            "criteria": {"status": "finished"},
            "callback": {
                "url": "https://federation.lavasoftware.org/api/v0.1/jobs/",
                "token": "my_token",
                "method": "POST",
                "dataset": "all",
                "content-type": "json",
            },
        }
