# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import pytest

from lavafed.schema import lab_schema, master_schema


def test_lab_schema():
    schema = lab_schema()
    with pytest.raises(Exception):
        schema({})

    schema(
        {
            "url": "http://example.com",
            "job_url": "http://example.com/scheduler/job/{id}",
            "username": "self",
            "token": "my_token",
            "arch": "amd64",
            "worker_token": "my-token",
            "slot": {
                "every": "day",
                "starting": "1:00",
                "timezone": "UTC",
                "duration": "1h",
            },
            "devices": [],
        }
    )
    schema(
        {
            "url": "http://example.com",
            "job_url": "http://example.com/scheduler/job/{id}",
            "username": "self",
            "token": "my_token",
            "arch": "amd64",
            "worker_token": "my-token",
            "slot": {
                "every": "day",
                "starting": "1:00",
                "timezone": "UTC",
                "duration": "1h",
            },
            "devices": [],
            "dispatcher": {"something": "1"},
        }
    )

    with pytest.raises(Exception):
        schema(
            {
                "url": "http://example.com",
                "username": "self",
                "token": "my_token",
                "arch": "amd64",
                "worker_token": "my-token",
                "slot": {
                    "every": "day",
                    "starting": "1:00",
                    "timezone": "UTC",
                    "duration": "1h",
                },
                "devices": [],
                "a new key": 1,
            }
        )


def test_master_schema():
    schema = master_schema()
    with pytest.raises(Exception):
        schema({})

    schema(
        {
            "job_url": "http://example.com/lava/scheduler/job/{id}",
            "url": "http://example.com",
            "username": "self",
            "token": "my_token",
            "api": {"token": "my token", "url": "http://example.com/api"},
        }
    )

    with pytest.raises(Exception):
        schema(
            {
                "a new key": "not allowed",
                "job_url": "http://example.com/lava/scheduler/job/{id}",
                "url": "http://example.com",
                "username": "self",
                "token": "my_token",
                "api": {"token": "my token", "url": "http://example.com/api"},
            }
        )
