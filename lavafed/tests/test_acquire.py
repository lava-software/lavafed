# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

import logging
import time
import xmlrpc.client

from django.core.management import call_command
import pytest

from lavafed import utils
from lavafed.models import Device, Job


@pytest.mark.django_db
def test_acquire(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: True)
    caplog.set_level(logging.DEBUG)

    # This should be tested in another test
    def fake_copy_device(device, master, lab, devices_cfg, log):
        assert device.name in ["device-01", "device-03"]  # nosec - pytest
        if device.name == "device-01":
            assert device.dt.name == "dt-01"  # nosec - pytest
        if device.name == "device-03":
            assert device.dt.name == "dt-lava-03"  # nosec - pytest
        assert devices_cfg == [  # nosec - pytest
            {"name": "device-01"},
            {"name": "device-02"},
            {"name": "device-03", "device-type": "dt-lava-03"},
        ]

    monkeypatch.setattr(utils, "copy_device", fake_copy_device)

    # This should be tested in another test
    def fake_wait_for_devices(proxy, log, devices, b):
        assert b is False  # nosec - pytest
        assert len(devices) == 2  # nosec - pytest
        assert devices[0].name == "device-01"  # nosec - pytest
        yield devices[0]
        assert devices[1].name == "device-03"  # nosec - pytest
        yield devices[1]

    monkeypatch.setattr(utils, "wait_for_devices", fake_wait_for_devices)
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "master",
        [
            {"request": "system.version", "args": (), "ret": "2018.10.0032.g0f7a7ec8f"},
            {
                "request": "scheduler.workers.show",
                "args": ("lava.test",),
                "ret": {"state": "Offline"},
            },
            {
                "request": "scheduler.workers.show",
                "args": ("lava.test",),
                "ret": {"state": "Online"},
            },
            {
                "request": "scheduler.jobs.submit",
                "args": (
                    [
                        "device_type: qemu",
                        'job_name: "[lavafed 2018.10.0032.g0f7a7ec8f] qemu - health-check"',
                        'device.name: "device-01"',
                        'device.type: "qemu"',
                        'job.url: "https://federation.lavasoftware.org/lava/scheduler/job/{id}"',
                        'job.name: "qemu - health-check"',
                        'job.type: "test"',
                        'lab.name: "lava.test"',
                        'slave.arch: "aarch64"',
                        'slave.version: "2018.10.0032.g0f7a7ec8f"',
                        'features.0.name: "deploy.tmpfs"',
                        'features.0.type: "device"',
                        'features.0.description: "Deploy to tmpfs"',
                        'features.0.action: "deploy"',
                        'features.1.name: "boot.qemu"',
                        'features.1.type: "device"',
                        'features.1.description: "Boot qemu"',
                        'features.1.action: "boot"',
                        'features.2.name: "test.definition.git"',
                        'features.2.type: "device"',
                        'features.2.description: "Test definition from git"',
                        'features.2.action: "test"',
                        'features.3.name: "test.definition.inline"',
                        'features.3.type: "device"',
                        'features.3.description: "Inline test definition"',
                        'features.3.action: "test"',
                    ],
                ),
                "ret": 42,
                "approx": True,
            },
            {
                "request": "scheduler.devices.show",
                "args": ("device-01@lava.test",),
                "ret": {"health": "Maintenance"},
            },
            {
                "request": "scheduler.devices.update",
                "args": (
                    "device-01@lava.test",
                    None,
                    None,
                    None,
                    None,
                    "UNKNOWN",
                    None,
                ),
                "ret": None,
            },
            {
                "request": "scheduler.devices.show",
                "args": ("device-03@lava.test",),
                "ret": {"health": "Maintenance"},
            },
            {
                "request": "scheduler.devices.update",
                "args": (
                    "device-03@lava.test",
                    None,
                    None,
                    None,
                    None,
                    "UNKNOWN",
                    None,
                ),
                "ret": None,
            },
        ],
    )
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "lab",
        [
            {"request": "system.version", "args": (), "ret": "2018.10.0032.g0f7a7ec8f"},
            {
                "request": "scheduler.jobs.submit",
                "args": (
                    [
                        'job_name: "[lavafed 2018.10.0032.g0f7a7ec8f',
                        'device.name: "lava-slave"',
                        'device.type: "lava-slave"',
                        'job.url: "https://lava.test/scheduler/job/{id}"',
                        'job.name: "lava-slave"',
                        'job.type: "slave"',
                        'lab.name: "lava.test"',
                        'slave.arch: "aarch64"',
                        'slave.version: "2018.10.0032.g0f7a7ec8f"',
                        'features.0.name: "master-slave"',
                        'features.0.type: "generic"',
                        'features.0.description: "master-slave protocol"',
                        'features.0.action: "None"',
                    ],
                ),
                "ret": 1,
                "approx": True,
            },
            {
                "request": "scheduler.jobs.show",
                "args": (1,),
                "ret": {"state": "Running"},
            },
            {
                "request": "scheduler.devices.show",
                "args": ("device-01",),
                "ret": {"health": "Good", "device_type": "dt-01"},
            },
            {
                "request": "scheduler.devices.update",
                "args": ("device-01", None, None, None, None, "MAINTENANCE", None),
                "ret": None,
            },
            {
                "request": "scheduler.devices.show",
                "args": ("device-02",),
                "ret": {"health": "Maintenance"},
            },
            {
                "request": "scheduler.devices.show",
                "args": ("device-03",),
                "ret": {"health": "Unknown", "device_type": "dt-03"},
            },
            {
                "request": "scheduler.devices.update",
                "args": ("device-03", None, None, None, None, "MAINTENANCE", None),
                "ret": None,
            },
        ],
    )
    call_command("acquire", "lava.test", "--config", tmpdir)
    assert xmlrpc.client.ServerProxy.master == []  # nosec - pytest
    assert xmlrpc.client.ServerProxy.lab == []  # nosec - pytest
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 10, "[INIT] * labs/lava.test/config.yaml"),
        ("lavafed", 20, "[INIT] Start xmlrpc proxies"),
        ("lavafed", 20, "[INIT] * master 2018.10.0032.g0f7a7ec8f"),
        ("lavafed", 20, "[INIT] * lab 2018.10.0032.g0f7a7ec8f"),
        ("lavafed", 20, "[lab] submit slave"),
        ("lavafed", 10, "-> id: 1"),
        ("lavafed", 20, "[lab] maintenance:"),
        ("lavafed", 20, "- device-01"),
        ("lavafed", 20, "- device-02"),
        ("lavafed", 30, "--> health is not good, skip (Maintenance)"),
        ("lavafed", 20, "- device-03"),
        ("lavafed", 20, "[master] add in maintenance:"),
        ("lavafed", 20, "- device-01"),
        ("lavafed", 20, "- device-03"),
        ("lavafed", 20, "[master] submit:"),
        ("lavafed", 20, "- device-01"),
        ("lavafed", 20, "--> hc.jinja2 (qemu - health-check)"),
        ("lavafed", 10, "---> 42"),
        ("lavafed", 20, "- device-03"),
        ("lavafed", 20, "[lab] waiting for jobs to end:"),
        ("lavafed", 20, "--> [master] acquire 'device-01@lava.test'"),
        ("lavafed", 20, "--> [master] acquire 'device-03@lava.test'"),
    ]

    assert Job.objects.count() == 2  # nosec - pytest
    (j1, j2) = Job.objects.all()
    assert j1.name == "lava-slave"  # nosec - pytest
    assert j1.device.name == "lava-slave"  # nosec - pytest
    assert j1.version.version == "2018.10.0032.g0f7a7ec8f"  # nosec - pytest
    assert j1.get_arch_display() == "aarch64"  # nosec - pytest
    assert j1.get_type_display() == "SLAVE"  # nosec - pytest
    assert j1.get_health_display() == "Unknown"  # nosec - pytest
    assert j1.lava == 1  # nosec - pytest
    assert j1.url == ""  # nosec - pytest
    assert j2.name == "qemu - health-check"  # nosec - pytest
    assert j2.get_arch_display() == "aarch64"  # nosec - pytest
    assert j2.version.version == "2018.10.0032.g0f7a7ec8f"  # nosec - pytest
    assert j2.get_type_display() == "FED"  # nosec - pytest
    assert j2.get_health_display() == "Unknown"  # nosec - pytest
    assert j2.lava == 42  # nosec - pytest
    assert j2.url == ""  # nosec - pytest

    assert Device.objects.count() == 3  # nosec
    (d1, d2, d3) = Device.objects.all()
    assert d1.name == "lava-slave"  # nosec
    assert d2.name == "device-01"  # nosec
    assert d2.dt.name == "dt-01"  # nosec
    assert d2.lab.name == "lava.test"  # nosec
    assert d2.acquired  # nosec
    assert d2.health == Device.HEALTH_GOOD  # nosec
    assert d3.name == "device-03"  # nosec
    assert d3.dt.name == "dt-lava-03"  # nosec
    assert d3.lab.name == "lava.test"  # nosec
    assert d3.acquired  # nosec
    assert d3.health == Device.HEALTH_UNKNOWN  # nosec


@pytest.mark.django_db
def test_acquire_missing_master_cfg(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: True)
    caplog.set_level(logging.DEBUG)

    (tmpdir / "master.yaml").remove()
    call_command("acquire", "lava.test", "--config", tmpdir)
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 40, "[INIT] => file does not exist"),
    ]


@pytest.mark.django_db
def test_acquire_invalid_master_cfg(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: True)
    caplog.set_level(logging.DEBUG)

    (tmpdir / "master.yaml").write_text("", encoding="utf-8")
    call_command("acquire", "lava.test", "--config", tmpdir)
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 40, "expected a dictionary"),
        ("lavafed", 40, "[INIT] => invalid configuration"),
    ]


@pytest.mark.django_db
def test_acquire_missing_lab_cfg(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: True)
    caplog.set_level(logging.DEBUG)

    (tmpdir / "labs" / "lava.test" / "config.yaml").remove()
    call_command("acquire", "lava.test", "--config", tmpdir)
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 10, "[INIT] * labs/lava.test/config.yaml"),
        ("lavafed", 40, "[INIT] => file does not exist"),
    ]


@pytest.mark.django_db
def test_acquire_invalid_lab_cfg(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: True)
    caplog.set_level(logging.DEBUG)

    (tmpdir / "labs" / "lava.test" / "config.yaml").write_text("", encoding="utf-8")
    call_command("acquire", "lava.test", "--config", tmpdir)
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 10, "[INIT] * labs/lava.test/config.yaml"),
        ("lavafed", 40, "expected a dictionary"),
        ("lavafed", 40, "[INIT] => invalid configuration"),
    ]


@pytest.mark.django_db
def test_acquire_not_in_slot(caplog, monkeypatch, setup, tmpdir):
    monkeypatch.setattr(time, "sleep", lambda duration: None)
    monkeypatch.setattr(utils.Slot, "is_allowed", lambda self: False)
    caplog.set_level(logging.DEBUG)

    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "master",
        [{"request": "system.version", "args": (), "ret": "2018.10.0032.g0f7a7ec8f"}],
    )
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "lab",
        [{"request": "system.version", "args": (), "ret": "2018.10.0032.g0f7a7ec8f"}],
    )

    call_command("acquire", "lava.test", "--config", tmpdir)
    assert xmlrpc.client.ServerProxy.master == []  # nosec - pytest
    assert xmlrpc.client.ServerProxy.lab == []  # nosec - pytest
    assert caplog.record_tuples == [  # nosec - pytest
        ("lavafed", 20, "[INIT] Loading config '%s'" % tmpdir),
        ("lavafed", 10, "[INIT] * master.yaml"),
        ("lavafed", 10, "[INIT] * labs/lava.test/config.yaml"),
        ("lavafed", 20, "[INIT] Start xmlrpc proxies"),
        ("lavafed", 20, "[INIT] * master 2018.10.0032.g0f7a7ec8f"),
        ("lavafed", 20, "[INIT] * lab 2018.10.0032.g0f7a7ec8f"),
        ("lavafed", 20, "Not allowed to run now"),
    ]
