# -*- coding: utf-8 -*-
# vim: set ts=4

# Copyright 2018 Rémi Duraffort
# This file is part of lavafed.
#
# lavafed is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lavafed is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with lavafed.  If not, see <http://www.gnu.org/licenses/>

from datetime import datetime
import logging
import pytest
import pytz
import xmlrpc.client

from django.utils import timezone

from lavafed.models import Device, DeviceType, Lab
from lavafed.utils import (
    build_docker_version,
    build_proxy,
    build_uri,
    copy_device,
    get_device_config,
    Slot,
    suppress404,
)

from .conftest import RecordingProxyFactory


def test_slot_every_day(monkeypatch):
    # Test each day of the week (2018/11/5 is a Monday)
    for day in Slot.DAYS:
        monkeypatch.setattr(
            timezone,
            "now",
            lambda: datetime(2018, 11, 5 + Slot.DAYS[day], 1, 5).replace(
                tzinfo=pytz.utc
            ),
        )
        s = Slot(
            {"every": day, "starting": "1:00", "timezone": "UTC", "duration": "2h25m"}
        )
        assert s.is_allowed()  # nosec - pytest
        for i in range(1, 7):
            monkeypatch.setattr(
                timezone,
                "now",
                lambda: datetime(2018, 11, 5 + Slot.DAYS[day] + i, 1, 5).replace(
                    tzinfo=pytz.utc
                ),
            )
            assert not s.is_allowed()  # nosec - pytest


def test_slot_everyday(monkeypatch):
    s = Slot(
        {"every": "day", "starting": "1:00", "timezone": "UTC", "duration": "2h25m"}
    )
    for day in Slot.DAYS:
        for i in range(0, 7):
            monkeypatch.setattr(
                timezone,
                "now",
                lambda: datetime(2018, 11, 5 + Slot.DAYS[day] + i, 1, 5).replace(
                    tzinfo=pytz.utc
                ),
            )
            assert s.is_allowed()  # nosec - pytest


def test_slot_every_weekday(monkeypatch):
    s = Slot(
        {"every": "weekday", "starting": "1:00", "timezone": "UTC", "duration": "2h25m"}
    )
    for i in range(0, 5):
        monkeypatch.setattr(
            timezone,
            "now",
            lambda: datetime(2018, 11, 5 + i, 1, 5).replace(tzinfo=pytz.utc),
        )
        assert s.is_allowed()  # nosec - pytest
    for i in range(5, 7):
        monkeypatch.setattr(
            timezone,
            "now",
            lambda: datetime(2018, 11, 5 + i, 1, 5).replace(tzinfo=pytz.utc),
        )
        assert not s.is_allowed()  # nosec - pytest


def test_slot_every_weekend(monkeypatch):
    s = Slot(
        {"every": "weekend", "starting": "1:00", "timezone": "UTC", "duration": "2h25m"}
    )
    for i in range(0, 5):
        monkeypatch.setattr(
            timezone,
            "now",
            lambda: datetime(2018, 11, 5 + i, 1, 5).replace(tzinfo=pytz.utc),
        )
        assert not s.is_allowed()  # nosec - pytest
    for i in range(5, 7):
        monkeypatch.setattr(
            timezone,
            "now",
            lambda: datetime(2018, 11, 5 + i, 1, 5).replace(tzinfo=pytz.utc),
        )
        assert s.is_allowed()  # nosec - pytest


def test_remaining_time(monkeypatch):
    # On a Monday
    monkeypatch.setattr(
        timezone, "now", lambda: datetime(2018, 11, 5, 1, 5).replace(tzinfo=pytz.utc)
    )
    s = Slot(
        {"every": "monday", "starting": "1:00", "timezone": "UTC", "duration": "2h25m"}
    )
    assert s.is_allowed()  # nosec - pytest
    assert s.remaining_time() == 2 * 3600 + 25 * 60 - 5 * 60  # nosec - pytest

    # On a Monday with a different timezone (one hour ahead)
    s = Slot(
        {
            "every": "monday",
            "starting": "1:00",
            "timezone": "Europe/Paris",
            "duration": "2h25m",
        }
    )
    assert s.is_allowed()  # nosec - pytest
    assert s.remaining_time() == 1 * 3600 + 25 * 60 - 5 * 60  # nosec - pytest

    # Les than 1h
    s = Slot(
        {
            "every": "monday",
            "starting": "00:00",
            "timezone": "Europe/Paris",
            "duration": "2h25m",
        }
    )
    assert s.is_allowed()  # nosec - pytest
    assert s.remaining_time() == 25 * 60 - 5 * 60  # nosec - pytest

    # time is over
    s = Slot(
        {
            "every": "monday",
            "starting": "00:00",
            "timezone": "Europe/Paris",
            "duration": "25m",
        }
    )
    assert not s.is_allowed()  # nosec - pytest
    assert s.remaining_time() == 0  # nosec - pytest

    # next Monday
    monkeypatch.setattr(
        timezone, "now", lambda: datetime(2018, 11, 12, 1, 5).replace(tzinfo=pytz.utc)
    )
    s = Slot(
        {
            "every": "monday",
            "starting": "1:00",
            "timezone": "Europe/Paris",
            "duration": "2h25m",
        }
    )
    assert s.is_allowed()  # nosec - pytest
    assert s.remaining_time() == 1 * 3600 + 25 * 60 - 5 * 60  # nosec - pytest


def test_build_docker_version():
    assert (  # nosec - pytest
        build_docker_version("2018.10.42.g8e7c50edb+stretch") == "2018.10.42.g8e7c50edb"
    )
    assert (  # nosec - pytest
        build_docker_version("2018.10.42.g8e7c50edb+buster") == "2018.10.42.g8e7c50edb"
    )
    assert build_docker_version("2018.10+stretch") == "2018.10"  # nosec - pytest
    assert build_docker_version("2018.10+buster") == "2018.10"  # nosec - pytest
    assert build_docker_version("2018.10") == "2018.10"  # nosec - pytest
    with pytest.raises(Exception):
        assert build_docker_version("2018")  # nosec - pytest


def test_build_uri():
    assert build_uri("https://example.com/RPC2", "user", "token") == (  # nosec - pytest
        "https",
        "https://user:token@example.com/RPC2",
    )
    assert build_uri("http://example.com/RPC2", "user", "token") == (  # nosec - pytest
        "http",
        "http://user:token@example.com/RPC2",
    )
    assert build_uri(  # nosec - pytest
        "https://example.com:8443/RPC2", "user", "token"
    ) == ("https", "https://user:token@example.com:8443/RPC2")


@pytest.mark.django_db
def test_copy_device(caplog, monkeypatch):
    monkeypatch.setattr(xmlrpc.client, "ServerProxy", RecordingProxyFactory())
    caplog.set_level(logging.DEBUG)
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "master",
        [
            {"request": "scheduler.device_types.list", "args": (), "ret": []},
            {
                "request": "scheduler.device_types.add",
                "args": ("dt-01", "", True, False, 24, "hours"),
                "ret": None,
            },
            {"request": "scheduler.devices.list", "args": (), "ret": []},
            {
                "request": "scheduler.devices.add",
                "args": (
                    "device-01@lab.test",
                    "dt-01",
                    "lab.test",
                    None,
                    None,
                    True,
                    "MAINTENANCE",
                    None,
                ),
                "ret": [],
            },
            {
                "request": "scheduler.devices.tags.add",
                "args": ("device-01@lab.test", "lab.test"),
                "ret": [],
            },
            {
                "request": "scheduler.devices.set_dictionary",
                "args": ("device-01@lab.test", "{% extends 'dt-01.jinja2' %}"),
                "ret": [],
            },
        ],
    )
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "lab",
        [
            {
                "request": "scheduler.devices.show",
                "args": ("device-01",),
                "ret": {"health": "Good", "device_type": "dt-01"},
            },
            {
                "request": "scheduler.devices.get_dictionary",
                "args": ("device-01", False),
                "ret": "{% extends 'dt-01.jinja2' %}",
            },
        ],
    )
    lab = Lab.objects.create(name="lab.test", url="http://lab.test.org")
    dt = DeviceType.objects.create(name="dt-01")
    device = Device.objects.create(name="device-01", dt=dt, lab=lab, acquired=False)
    lab_proxy = build_proxy(
        {
            "url": "https://lava.test/RPC2",
            "username": "lava_lab_user",
            "token": "lava_lab_token",
        }
    )
    master_proxy = build_proxy(
        {
            "url": "https://federation.lavasoftware.org/lava/RPC2/",
            "username": "lava_master_user",
            "token": "lava_master_token",
        }
    )
    devices_cfg = [
        {"name": "device-01"},
        {"name": "device-02", "patches": [{"from": "localhost", "to": "external"}]},
    ]
    copy_device(device, master_proxy, lab_proxy, devices_cfg, logging.getLogger())
    assert xmlrpc.client.ServerProxy.master == []  # nosec - pytest
    assert xmlrpc.client.ServerProxy.lab == []  # nosec - pytest
    assert caplog.record_tuples == [  # nosec - pytest
        ("root", 10, "--> create device-type 'dt-01'"),
        ("root", 10, "--> add device as 'device-01@lab.test'"),
        ("root", 10, "--> add tag 'lab.test'"),
        ("root", 10, "--> update device dict"),
    ]


@pytest.mark.django_db
def test_copy_device_device_type(caplog, monkeypatch):
    monkeypatch.setattr(xmlrpc.client, "ServerProxy", RecordingProxyFactory())
    caplog.set_level(logging.DEBUG)
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "master",
        [
            {"request": "scheduler.device_types.list", "args": (), "ret": []},
            {
                "request": "scheduler.device_types.add",
                "args": ("real-device-type", "", True, False, 24, "hours"),
                "ret": None,
            },
            {"request": "scheduler.devices.list", "args": (), "ret": []},
            {
                "request": "scheduler.devices.add",
                "args": (
                    "device-02@lab.test",
                    "real-device-type",
                    "lab.test",
                    None,
                    None,
                    True,
                    "MAINTENANCE",
                    None,
                ),
                "ret": [],
            },
            {
                "request": "scheduler.devices.tags.add",
                "args": ("device-02@lab.test", "lab.test"),
                "ret": [],
            },
            {
                "request": "scheduler.devices.set_dictionary",
                "args": (
                    "device-02@lab.test",
                    "{% extends 'dt-01.jinja2' %}\n{% set power_on = \"telnet localhost 7501\" %}",
                ),
                "ret": [],
            },
        ],
    )
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "lab",
        [
            {
                "request": "scheduler.devices.show",
                "args": ("device-02",),
                "ret": {"health": "Good", "device_type": "dt-01"},
            },
            {
                "request": "scheduler.devices.get_dictionary",
                "args": ("device-02", False),
                "ret": "{% extends 'dt-01.jinja2' %}\n{% set power_on = \"telnet localhost 7501\" %}",
            },
        ],
    )
    lab = Lab.objects.create(name="lab.test", url="http://lab.test.org")
    dt = DeviceType.objects.create(name="dt-01")
    device = Device.objects.create(name="device-02", dt=dt, lab=lab, acquired=False)
    lab_proxy = build_proxy(
        {
            "url": "https://lava.test/RPC2",
            "username": "lava_lab_user",
            "token": "lava_lab_token",
        }
    )
    master_proxy = build_proxy(
        {
            "url": "https://federation.lavasoftware.org/lava/RPC2/",
            "username": "lava_master_user",
            "token": "lava_master_token",
        }
    )
    devices_cfg = [
        {"name": "device-01"},
        {"name": "device-02", "device-type": "real-device-type"},
    ]
    copy_device(device, master_proxy, lab_proxy, devices_cfg, logging.getLogger())
    assert xmlrpc.client.ServerProxy.master == []  # nosec - pytest
    assert xmlrpc.client.ServerProxy.lab == []  # nosec - pytest
    assert caplog.record_tuples == [  # nosec - pytest
        ("root", 10, "--> create device-type 'real-device-type'"),
        ("root", 10, "--> add device as 'device-02@lab.test'"),
        ("root", 10, "--> add tag 'lab.test'"),
        ("root", 10, "--> update device dict"),
    ]


@pytest.mark.django_db
def test_copy_device_patch(caplog, monkeypatch):
    monkeypatch.setattr(xmlrpc.client, "ServerProxy", RecordingProxyFactory())
    caplog.set_level(logging.DEBUG)
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "master",
        [
            {"request": "scheduler.device_types.list", "args": (), "ret": []},
            {
                "request": "scheduler.device_types.add",
                "args": ("dt-01", "", True, False, 24, "hours"),
                "ret": None,
            },
            {"request": "scheduler.devices.list", "args": (), "ret": []},
            {
                "request": "scheduler.devices.add",
                "args": (
                    "device-02@lab.test",
                    "dt-01",
                    "lab.test",
                    None,
                    None,
                    True,
                    "MAINTENANCE",
                    None,
                ),
                "ret": [],
            },
            {
                "request": "scheduler.devices.tags.add",
                "args": ("device-02@lab.test", "lab.test"),
                "ret": [],
            },
            {
                "request": "scheduler.devices.set_dictionary",
                "args": (
                    "device-02@lab.test",
                    "{% extends 'dt-01.jinja2' %}\n{% set power_on = \"telnet 10.1.1.2 7501\" %}",
                ),
                "ret": [],
            },
        ],
    )
    monkeypatch.setattr(
        xmlrpc.client.ServerProxy,
        "lab",
        [
            {
                "request": "scheduler.devices.show",
                "args": ("device-02",),
                "ret": {"health": "Good", "device_type": "dt-01"},
            },
            {
                "request": "scheduler.devices.get_dictionary",
                "args": ("device-02", False),
                "ret": "{% extends 'dt-01.jinja2' %}\n{% set power_on = \"telnet localhost 7501\" %}",
            },
        ],
    )
    lab = Lab.objects.create(name="lab.test", url="http://lab.test.org")
    dt = DeviceType.objects.create(name="dt-01")
    device = Device.objects.create(name="device-02", dt=dt, lab=lab, acquired=False)
    lab_proxy = build_proxy(
        {
            "url": "https://lava.test/RPC2",
            "username": "lava_lab_user",
            "token": "lava_lab_token",
        }
    )
    master_proxy = build_proxy(
        {
            "url": "https://federation.lavasoftware.org/lava/RPC2/",
            "username": "lava_master_user",
            "token": "lava_master_token",
        }
    )
    devices_cfg = [
        {"name": "device-01"},
        {
            "name": "device-02",
            "patches": [{"from": "telnet localhost", "to": "telnet 10.1.1.2"}],
        },
    ]
    copy_device(device, master_proxy, lab_proxy, devices_cfg, logging.getLogger())
    assert xmlrpc.client.ServerProxy.master == []  # nosec - pytest
    assert xmlrpc.client.ServerProxy.lab == []  # nosec - pytest
    assert caplog.record_tuples == [  # nosec - pytest
        ("root", 10, "--> create device-type 'dt-01'"),
        ("root", 10, "--> add device as 'device-02@lab.test'"),
        ("root", 10, "--> add tag 'lab.test'"),
        ("root", 10, "--> update device dict"),
    ]


def test_get_device_config():
    assert get_device_config(  # nosec - pytest
        [{"name": "device-01"}, {"name": "device-02", "patches": []}], "device-01"
    ) == {"name": "device-01"}
    assert get_device_config(  # nosec - pytest
        [{"name": "device-01"}, {"name": "device-02", "patches": []}], "device-02"
    ) == {"name": "device-02", "patches": []}


def test_suppress():
    with suppress404(None):
        raise xmlrpc.client.Fault(404, "something")
    with pytest.raises(xmlrpc.client.Fault):
        with suppress404(None):
            raise xmlrpc.client.Fault(500, "something")
