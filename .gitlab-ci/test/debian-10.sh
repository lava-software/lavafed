#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  apt-get update -q
  apt-get install --no-install-recommends --yes apache2 libapache2-mod-wsgi-py3 libjs-jquery python3 python3-django python3-jinja2 python3-requests python3-svgwrite python3-tz python3-voluptuous python3-yaml
  apt-get install --no-install-recommends --yes python3-pytest python3-pytest-cov python3-pytest-django
  django-admin startproject fed .
  echo "INSTALLED_APPS.append(\"lavafed\")" >> fed/settings.py
  cp share/urls.py fed/urls.py
else
  set -x
  pytest-3 -v --ds fed.settings --cov lavafed --cov-report html --cov-report term --junitxml=lavafed.xml lavafed/tests
fi
