#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  set -x
  apt-get update -qq
  apt-get install --no-install-recommends -y make python3 python3-pytest-runner python3-setuptools python3-sphinx
else
  set -x
  make -C doc html
fi
