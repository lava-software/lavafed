#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  true
else
  # Pull the image
  echo "Pulling $CI_REGISTRY_IMAGE:latest"
  docker pull "$CI_REGISTRY_IMAGE:latest"

  # Check if the container is running
  echo "Is a container called \"$LAVAFED_CONTAINER_NAME\" running?"
  version=$(docker container ls --filter name="$LAVAFED_CONTAINER_NAME" --format "{{.Image}}")
  if [ -n "$version" ]
  then
    echo "Stopping the container"
    docker container stop "$LAVAFED_CONTAINER_NAME"
    docker container rm "$LAVAFED_CONTAINER_NAME"
  fi

  echo "Starting \"$LAVAFED_CONTAINER_NAME\""
  cd "$LAVAFED_PATH"
  docker container run --name "$LAVAFED_CONTAINER_NAME" -d \
      --restart always \
      --add-host "postgresql:172.17.0.1" \
      -v /etc/lavafed/:/etc/lavafed/ \
      -p 9002:80 \
      -v "/home/lavafed/federation/etc/labs/:/app/etc/labs/" \
      -v "/home/lavafed/federation/etc/master.yaml:/app/etc/master.yaml" \
      "$CI_REGISTRY_IMAGE:latest"
fi
