#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  true
else
  mkdir -p ${HOME}/docs/lavafed
  rsync -av --delete doc/_build/html ${HOME}/docs/lavafed/
fi
