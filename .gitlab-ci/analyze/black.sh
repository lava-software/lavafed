#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  true
else
  set -x
  LC_ALL=C.UTF-8 LANG=C.UTF-8 black --check .
fi
