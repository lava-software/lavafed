FROM debian:buster-slim

LABEL maintainer="Rémi Duraffort <remi.duraffort@linaro.org>"

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt-get update -q ;\
    apt-get install --no-install-recommends --yes gunicorn3 libjs-jquery python3 python3-pip python3-django python3-jinja2 python3-psycopg2 python3-requests python3-svgwrite python3-tz python3-voluptuous python3-yaml ;\
    python3 -m pip install --upgrade whitenoise ;\
    python3 -m pip install --upgrade 'sentry-sdk==0.17.1' ;\
    # Cleanup
    apt-get clean ;\
    rm -rf /var/lib/apt/lists/*

# Add lavafed sources
WORKDIR /app/
COPY etc/ /app/etc/
COPY lavafed/ /app/lavafed/
COPY share/settings.py /app/fedweb/custom_settings.py
COPY share/urls.py /app/fedweb/urls.py.orig

# Create the django project
RUN chmod 775 /app ;\
    django-admin startproject fedweb /app ;\
    mv /app/fedweb/urls.py.orig /app/fedweb/urls.py ;\
    # Setup lavafed application
    echo "INSTALLED_APPS.append(\"lavafed\")" >> /app/fedweb/settings.py ;\
    echo "from fedweb.custom_settings import *" >> /app/fedweb/settings.py ;\
    # Migrate and collect static files
    python3 manage.py migrate --noinput ;\
    python3 manage.py collectstatic --noinput

COPY share/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
