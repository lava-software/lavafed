.. _index:

lavafed
#######

LAVA Federation is a project aiming at testing the LAVA software on community
owned hardware. The tests are spread across many labs with a variety of
hardware.

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   design
   install
   remote
