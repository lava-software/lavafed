.. _design:

Design
######

Overview
--------

Lavafed is made of three elements:

* the lavafed application
* the federated lava server
* remote labs

lavafed application
===================

This django application is the central point where the results are stored and
made visible to users.

This application is also responsible for acquiring and releasing the different
devices that are hosted in the remote labs.

The official `lavafed application <https://federation.lavasoftware.org/>`_ is
hosted by the lavasoftware project.


federated lava server
=====================

This instance of LAVA is the central point of the federated LAVA instance. Each
remote lab will start a dockerized lava-dispatcher (in a lava job) that will
connent back to the federated master, forming a temporary LAVA dispatcher with
real devices.

The official `federated lava server
<https://federation.lavasoftware.org/lava/>`_ is hosted by the lavasoftware
project.

remote labs
===========

Remote LAVA instances that share real devices with lavafed for a fixed amount
of time every day.

Currently, the folowing labs are participating in lavafed:

* http://lava.streamtester.net
* https://master.lavasoftware.org
* https://validation.linaro.org
* https://staging.validation.linaro.org
* https://validation.linaro.org

Database objects
----------------

The lavafed application is storing different objects in the database:

device
======

The device that lavafed use for testing LAVA versions.

This object is used to store and restore the status of the device in the remote lab.
A device is always linked to a device-type and a lab.

device-type
===========

The LAVA device-type of a given device.

This is used to group test jobs and features together as the results are only
relevant for device-types and not specific devices.

feature
=======

A LAVA feature that is being tested by a lavafed job.

Features are of two kinds currently:

* `generic`: generic LAVA feature (notifications, ...)
* `device`: device-type specific (booting using tftp and u-boot, ...)

Device features are split in three actions `deploy`, `boot`, `test`

job
===

lavafed test job that is linking all objects together.

The job can be of four types:

* `FED`: lavafed job on a rela device
* `SLAVE`: lavafed dispatcher on a remote lab dispatcher
* `META`: meta-lava tests
* `PACKAGE`: packaging tests

lab
===

The remote lab with name and url.

token
=====

lavafed token used to authenticate API calls.

This token is mainly used by LAVA when posting the job results as part of the notifications.

version
=======

The version string that is tested by lavafed.

Users can then group features, devic-types or jobs per version.
