.. _remote:

Adding a remote lab
###################

Remote lab
==========

In order to integrate a remote device into lavafed, the remote lava instance
should be configured properly.

docker device
-------------

In order to start a testing lava-dispatcher on the lab dispatcher, a docker device
should be available in the remote lab.

You can add and configure a docker device for lavafed with:

.. code-block:: shell

    lava-server manage device-types add docker
    lava-server manage devices add --device-type docker --worker <worker> docker-01

.. note:: The docker device should be running on the dispatcher that control
    the board that the remote lab is sharing.

The device dictionary for the docker device should be based on the following
one:

.. code-block:: jinja

    {% extends 'docker.jinja2' %}
    
    {% set docker_cpus = '2' %}
    {% set docker_memory = '1024M' %}
    {% set docker_devices = ["/dev/kvm:/dev/kvm"] %}
    {% set docker_volumes = [
            "/var/lib/lava/dispatcher/tmp:/var/lib/lava/dispatcher/tmp",
            "/boot:/boot",
            "/lib/modules:/lib/modules"] %}

Depending on the number of devices, `docker_cpus` and `docker_memory` should be
updated. As a rule of thumb:

.. code-block:: text

    docker_cpus = number of devices + 1
    docker_memory = (number of devices + 1 )* 512M

.. note:: if the current dispatcher is running inside a docker container,
    admins should bind `/var/run/docker.sock` to allow the dispatcher to start
    another container.

User and Group
--------------

lavafed will need a user, part of the `lavafed` group, with the rights to:

* submit jobs to docker device-types
* update the state of the devices to lend

This is translating to the following permissions:

.. code-block:: text

    lava_scheduler_app | device | Can change device
    lava_scheduler_app | test job | Can add test job
    lava_scheduler_app | test job | Can cancel or resubmit test jobs

lavafed does not need the password of this user, only a valid token to connect
to the XML-RPC api.

Tokens
------

The admin should create two tokens for the lavafed user.

The first token will be used by lavafed scripts to submit jobs and update
devices health using the XMLRPC API. This token will be called
``remote_lava_server_token`` in the rest of the documentation.

The second token is used to hold the lavafed api token when submitting the
results. This token should have:

.. code-block:: yaml

    description: federation.lavasoftware.org
    secret: {{ remote_lavafed_api_token }}

lavafed server
==============

On the lavafed server, the admin should create a new lab and token.

Lab
---

A new lab should be added in the `lab admin page
<https://federation.lavasoftware.org/admin/lavafed/lab/>`_.

.. code-block:: yaml

    name: validation.linaro.org
    url: https://validation.linaro.org/

Tokens
------

A new token should be created in the `token admin page
<https://federation.lavasoftware.org/admin/lavafed/token/>`_.

.. code-block:: yaml

    token: <auto generated>
    description: lavafed token for linaro lab
    lab: validation.linaro.org

This token will be called ``remote_lavafed_api_token`` in the rest of the
documentation.

A second token should be created to hold the ``lava_server_lavafed_token``

.. code-block:: yaml

    token: {{ lava_server_lavafed_token }}
    description: lavafed token for federation.l.o
    lab: validation.linaro.org

Configuration file
==================

The lab configuration in `etc/labs/remote.lab/master.yaml` should look like:

.. code-block:: yaml

    # xmlrpc configuration
    # The user should be able to administrate devices
    url: https://{{ lava_remote_lab_dns }} /RPC2/
    job_url: https://{{ lava_remote_lab_dns }}/scheduler/job/{id}
    username: lavafed
    token: "{{ remote_lava_server_token }}"
    arch: amd64

    # Time slot
    slot:
      every: day
      starting: "09:00"
      timezone: UTC
      duration: 1h

    # zmq certificates
    certificates:
      private: "{{ remote_zmq_private_certificate }}"
      public: "{{ remote_zmq_public_certificate }}"

    # If needed, set the dispatcher configuration
    dispatcher:
      dispatcher_ip: "{{ dispatcher_ip }}"
      prefix: lavafed-

    # List of devices to borrow
    devices:
    - name: "{{ device_name_01 }}"
    - name: "{{ device_name_02 }}"

Patching device dictionary
--------------------------

Under some circumstances, the device dictionary should be adapted to be used in
lavafed.

In order to patch the device dictionary, you should update the lab
``config.yaml``:

.. code-block:: yaml

    devices:
    - name: device-01
      patches:
      - from: '{% set connection_command = "telnet localhost 2001" %}'
        to: '{% set connection_command = "telnet dispatcher-01 2001" %}'

Device-type aliases
-------------------

In some labs, the device-type naming conventions can differ from the official LAVA device-type names.
The device-type name of a give device might not match any of the official LAVA
device-types even if they underlying device-type template is officialy supported.

In thi case, the official device-type should be specified in the ``config.yaml``:

.. code-block:: yaml

    devices:
    - name: device-02
      device-type: offical-lava-device-type
