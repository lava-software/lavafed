anybadge
django>=1.11,<2.0
jinja2
requests
svgwrite
voluptuous
pyyaml
whitenoise
